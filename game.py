#greeting (do not print, bc no response, use input and assign value)
name = input("Hi! What is your name?")

#guessing month(you used this just to introduce the question)
print("I shall guess your birthday!")
from random import randint

#set up loop here

for guess_number in range(1,6):
    #first generate bday guesses with randint
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    #GUESS repeatable
    print("Guess", guess_number, name, ", were you born in ", month_number,"/", year_number)

    answer = input("yes or no?")

    if answer == "yes":
        print("I am psychic!")
        exit()
    elif guess_number == 5:
        print("I hate the stars, I am going to go learn python, BYE!")
        exit()
    else:
        print("Sorry, the stars are difficult to read, I shall guess again!")
